import { Input, List } from "antd"
import { useTypedSelector } from "../../hooks/useTypedSelector"
import styles from "./UsersList.module.css"

const { Search } = Input

export const UsersList = () => {
  const { users } = useTypedSelector((state) => state.user)
  console.log(users)
  return (
    <List
      header={<Search placeholder="Search" allowClear />}
      bordered
      dataSource={users!}
      renderItem={(item) => <List.Item>{item.name}</List.Item>}
    />
  )
}

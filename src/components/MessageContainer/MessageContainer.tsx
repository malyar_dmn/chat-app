import { Layout } from "antd"
import React, { useState } from "react"
import { useTypedSelector } from "../../hooks/useTypedSelector"
import { IMessage } from "../../models/Message"
import { MessageItem } from "../MessageItem/MessageItem"

export const MessageContainer = () => {
  const [mockMessages, setMockMessages] = useState<IMessage[]>([
    {
      id: "1",
      userId: "1",
      username: "max",
      messageContent: "test message content",
    },
    {
      id: "2",
      userId: "2",
      username: "dmn",
      messageContent: "test message cjasjeontent2442342342",
    },
    {
      id: "3",
      userId: "1",
      username: "max",
      messageContent: "test message contenfajfseoifjsoft",
    },
    {
      id: "4",
      userId: "1",
      username: "max",
      messageContent: "test message contentefsefsefsfe",
    },
  ])

  const onMessagesScroll = () => {
    console.log("scrolling...")
  }

  return (
    <Layout.Content style={{ overflow: "scroll" }} onScroll={onMessagesScroll}>
      {mockMessages.map((message) => (
        <MessageItem message={message} key={message.id} />
      ))}
    </Layout.Content>
  )
}

import React, { FC } from "react"
import { IMessage } from "../../models/Message"
import styles from "./MessageItem.module.css"

interface Props {
  message: IMessage
}

export const MessageItem: FC<Props> = (message) => {
  console.log(message)
  return (
    <div className={styles.wrapper}>
      message text
      <span>date</span>
      <span>icon status</span>
    </div>
  )
}

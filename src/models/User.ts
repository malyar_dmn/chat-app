import { IMessage } from "./Message"

export interface IUser {
  id: string
  name: string
  status?: string
}

export interface IMessage {
  id: string
  userId: string
  username: string
  messageContent: string
  messageDate?: string
  messageStatus?: string
}

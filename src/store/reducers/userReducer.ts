import { IUser } from "../../models/User"

interface UserState {
  users: IUser[] | null
  loading: boolean
  selectedUserId: string
}

enum UserActionTypes {
  FETCH_USERS = "FETCH_USERS",
}

interface FetchUsersAction {
  type: UserActionTypes.FETCH_USERS
  payload: IUser[]
}

type UserActions = FetchUsersAction

const initialState: UserState = {
  users: [
    {
      id: Date.now().toString(),
      name: "user1",
    },
    { id: "2", name: "user2" },
  ],
  loading: true,
  selectedUserId: "",
}

export const userReducer = (
  state = initialState,
  action: UserActions
): UserState => {
  switch (action.type) {
    case UserActionTypes.FETCH_USERS:

    default:
      return state
  }
}

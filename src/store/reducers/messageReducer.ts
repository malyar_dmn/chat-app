import { IMessage } from "../../models/Message"

interface MessageState {
  messages: IMessage[]
  isLoading: boolean
}

enum MessageActionTypes {
  FETCH_MESSAGES = "FETCH_MESSAGES",
}

interface FetchMessagesAction {
  type: MessageActionTypes.FETCH_MESSAGES
  payload: IMessage[]
}

const initialState: MessageState = {
  messages: [],
  isLoading: true,
}

export type MessageActions = FetchMessagesAction

export const messageReducer = (
  state = initialState,
  action: MessageActions
): MessageState => {
  switch (action.type) {
    case MessageActionTypes.FETCH_MESSAGES:

    default:
      return state
  }
}

import { Layout } from "antd"
import React, { useEffect, useState } from "react"
import "./App.css"
import { MessageContainer } from "./components/MessageContainer/MessageContainer"
import { UsersList } from "./components/UsersList/UsersList"

function App() {
  const [isHeaderVisible, setHeaderIsVisible] = useState<boolean>(true)

  return (
    <Layout style={{ height: "100vh" }}>
      <Layout.Sider theme="light" width={300}>
        <UsersList />
      </Layout.Sider>
      <Layout>
        {isHeaderVisible && (
          <Layout.Header style={{ backgroundColor: "#ccc" }}>
            header
          </Layout.Header>
        )}
        <MessageContainer />
      </Layout>
    </Layout>
  )
}

export default App

import { IMessage } from "../models/Message"
import { IUser } from "../models/User"

export interface IApiService {
  getUsers(): Promise<IUser[]>
  getMessagesOfUser(userId: string): Promise<IMessage[]>
}

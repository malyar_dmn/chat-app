import { IMessage } from "../models/Message"
import { IUser } from "../models/User"
import { IApiService } from "./types"

class MockApiService implements IApiService {
  getUsers(): Promise<IUser[]> {
    return Promise.resolve([])
  }

  getMessagesOfUser(userId: string): Promise<IMessage[]> {
    throw new Error("not implemented")
  }
}

export default new MockApiService()
